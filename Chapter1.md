# Operating System Concepts

## CPU

1. single-process systems
    - one process will be memory resident so the OS would mainly be responsible for starting the memory- resident process by giving it control of the CPU
2. multitasking systems
    - multiple processes will be memory resident so the OS will maintain various queues of processes        

## Main memory and caches

The OS needs to assign memory space to a process before it can execute

## Secondary storage

Files are stored on hard disk until there is a request to load some parts of them into main memory

## I/O

The OS must also control and manage the various input and output devices connected to a computer system

- device driver
    + handles low-level interaction with the device controllers, and presents a higher-level view of the I/O devices to the rest of the OS       

## File systems

File system is higher-level resource that are created through software        

## User interfaces        

User interfaces is a high-level component to handle user interaction        

## Network access        

An OS can provide both low- and high-level functionality for network access        

## Providing protection and security        

The OS also provides mechanisms to protect the various resources from unauthorized access, as well as security techniques to allow the system administrators to enforce their security policies        

## The Process Concepts

A process (sometimes called job or task) is basically a running or an executing program, and the OS manages system resources on behalf of the processes        

1. Process
    - running program
    - ready to run        
    - waiting                
        + waiting for I/O
        + waiting for the OS to assign it some resource    
2. program counter
    - specifies the location of the next instruction to be executed
3. Process State
    - new state
        + creating a new process
    - ready state
        + the process is ready to execute
    - running state
        + the process is executing
    - terminated state
        + the OS will do cleanup operations on the process
    - wait state
        + the process remains in the wait state until the resource it needs is allocated to it or its I/O request is completed
    - blocked state
4. Type of processes execution modes
    - User or application processes
    - Systems program processes
    - OS processes
    - Two execution modes for processes
        + privileged mode, also known as  supervisor mode, kernel mode,  or  monitor mode
            * allowing them to execute all types of hardware operations and to access all of memory and I/O devices
        + user mode
            * prohibits them from executing some commands such as low-level I/O commands
5. Functional Classes of OSs
    - single-user single-tasking OS
        + runs a single process at a time
            * CP/M
            * MS-DOS
    - multitasking or multiprogramming OS
        + control multiple processes running concurrently
6. Architecture Approaches to building an OS
    - Monolithic single-kernel OS approach
        + The first OSs were written as a single program
    - Layered OS approach
        + The OS would be divided into modules that were limited to a specific function such as processor scheduling or memory management















 
















